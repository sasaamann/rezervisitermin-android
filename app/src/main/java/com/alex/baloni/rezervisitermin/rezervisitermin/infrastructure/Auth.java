package com.alex.baloni.rezervisitermin.rezervisitermin.infrastructure;

import android.content.Context;

import com.alex.baloni.rezervisitermin.rezervisitermin.models.User;

public class Auth {

    private final Context context;

    private User user;

    public Auth(Context context) {
        this.context = context;
        user = new User();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }

    public void logoutUser() {
        this.user = new User();
        this.user.setLoggedIn(false);
    }
}
