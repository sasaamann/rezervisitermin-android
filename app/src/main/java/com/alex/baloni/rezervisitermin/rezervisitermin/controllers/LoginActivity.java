package com.alex.baloni.rezervisitermin.rezervisitermin.controllers;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.alex.baloni.rezervisitermin.rezervisitermin.R;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.User;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.TokenResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.services.HttpService;
import com.alex.baloni.rezervisitermin.rezervisitermin.services.RezervisiTerminApi;
import com.alex.baloni.rezervisitermin.rezervisitermin.utils.ISharedPref;
import com.alex.baloni.rezervisitermin.rezervisitermin.utils.SharedPrefUtil;
import com.crowdfire.cfalertdialog.CFAlertDialog;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.loginAct_loginBtn)
    CircularProgressButton loginBtn;

    @BindView(R.id.loginAct_usernameEt)
    EditText usernameEt;

    @BindView(R.id.loginAct_passwordEt)
    EditText passwordEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        //TODO: Delete manual input in production

        usernameEt.setText("info@rezervisitermin.com");
        passwordEt.setText("Rezervisi123!");


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameEt.getText().toString();
                String password = passwordEt.getText().toString();

                if(username.isEmpty() || password.isEmpty()){
                    showErrorDialog("Sva polja su obavezna");
                    return;
                }

                doLoginRetrofit(username, password);
            }
        });

    }


    public void doLoginRetrofit(String username, String password){
        loginBtn.startAnimation();

        RequestBody userData = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("username", username)
                .addFormDataPart("password", password)
                .addFormDataPart("client_id", "2")
                .addFormDataPart("client_secret", "M4yD7uzhRMiszgutbSyQ36uioHr7dHnBoB0VBVi6")
                .addFormDataPart("grant_type", "password")
                .build();

        final RezervisiTerminApi apiService = HttpService.getInstance().getApiService();

        apiService.getToken(userData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<TokenResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(TokenResponse tokenResponse) {
                        loginBtn.revertAnimation();
                        saveToken(tokenResponse, new SharedPrefUtil(getApplicationContext()));
                        startActivity(new Intent(LoginActivity.this, SplashActivity.class));
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        loginBtn.revertAnimation();
                        showErrorDialog("Doslo je do greske, molimo Vas provjerite svoje podatke");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void saveToken(TokenResponse tokenResponse, ISharedPref sharedPref) {
        sharedPref.saveUserToken(tokenResponse.getAccessToken());
    }

    private void showErrorDialog(String errorMessage) {
        // Create Alert using Builder
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                .setTitle("Greška")
                .setMessage(errorMessage)
                .addButton("U redu", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }
}
