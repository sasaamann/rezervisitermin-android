package com.alex.baloni.rezervisitermin.rezervisitermin.infrastructure;

import com.alex.baloni.rezervisitermin.rezervisitermin.utils.ISharedPref;

public interface ICheckForLoggedUser {
    boolean isUserExists(ISharedPref sharedPrefUtil);
}
