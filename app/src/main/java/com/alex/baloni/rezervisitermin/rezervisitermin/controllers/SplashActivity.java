
package com.alex.baloni.rezervisitermin.rezervisitermin.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.alex.baloni.rezervisitermin.rezervisitermin.R;
import com.alex.baloni.rezervisitermin.rezervisitermin.infrastructure.INavigator;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.User;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.TokenResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.services.HttpService;
import com.alex.baloni.rezervisitermin.rezervisitermin.services.RezervisiTerminApi;
import com.alex.baloni.rezervisitermin.rezervisitermin.utils.ISharedPref;
import com.alex.baloni.rezervisitermin.rezervisitermin.utils.SharedPrefUtil;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SplashActivity extends BaseActivity implements INavigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        String userToken = getUserToken(new SharedPrefUtil(getApplicationContext()));
        if(userToken != null && !userToken.isEmpty()){
            getUserData(userToken);
        }
        else{
            Toast.makeText(getApplicationContext(), "Doslo je do greske, molimo vas pokusajte ponovo", Toast.LENGTH_SHORT).show();
            navigateToActivity(LoginActivity.class);
        }
    }

    private String getUserToken(ISharedPref sharedPrefUtil) {
        return sharedPrefUtil.getUserToken();
    }

    private void getUserData(String token){
        final RezervisiTerminApi apiService = HttpService.getInstance().getApiService();

        apiService.getUserData("Bearer " + token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<User>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(User user) {
                        saveUserAndAuth(user, token);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getApplicationContext(), "Doslo je do greske, molimo vas pokusajte ponovo", Toast.LENGTH_SHORT).show();
                        navigateToActivity(LoginActivity.class);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void saveUserAndAuth(User loggedUser, String token) {
        //Save User
        User user = loggedUser;
        user.setClientToken(token);

        //Auth User
        application.getAuth().setUser(user);
        application.getAuth().getUser().setLoggedIn(true);

        //Navigate to main
        navigateToActivity(MainActivity.class);
    }


    @Override
    public void navigateToActivity(Class<?> classWhere) {
        Intent intent = new Intent(SplashActivity.this, classWhere);
        //Clear all activities on top
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        finish();
    }
}
