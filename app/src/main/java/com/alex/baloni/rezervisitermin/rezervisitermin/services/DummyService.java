package com.alex.baloni.rezervisitermin.rezervisitermin.services;

import android.app.Activity;
import android.content.Context;

import com.alex.baloni.rezervisitermin.rezervisitermin.R;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientReservationsResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientReservationsWrapperResponse;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DummyService {

    public ArrayList<ClientReservationsResponse> getReservations(Context context) throws IOException {
        BufferedReader reader = null;

        reader = new BufferedReader(new InputStreamReader(context.getAssets().open("reservations.json"), "UTF-8"));

        String content = "";
        String line;
        if (reader != null) {
            while ((line = reader.readLine()) != null)
            {
                content = content + line;
            }
        }



        ClientReservationsWrapperResponse clientReservations = new Gson().fromJson(content, ClientReservationsWrapperResponse.class);
        return clientReservations.getData();

    }

}
