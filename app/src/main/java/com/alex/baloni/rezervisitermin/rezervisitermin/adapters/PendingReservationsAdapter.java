package com.alex.baloni.rezervisitermin.rezervisitermin.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.alex.baloni.rezervisitermin.rezervisitermin.R;
import com.alex.baloni.rezervisitermin.rezervisitermin.helpers.IAdatpterClickListener;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientReservationsResponse;

import java.util.ArrayList;

public class PendingReservationsAdapter extends RecyclerView.Adapter<PendingReservationsAdapter.PendingReservationsViewHolder> {
    private ArrayList<ClientReservationsResponse> mClientReservationsResponses;
    private Context mContext;
    private IAdatpterClickListener listener;
    private int lastPosition = -1;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class PendingReservationsViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mClientName;
        public TextView mField;
        public TextView mTime;

        private View rootView;

        public PendingReservationsViewHolder(View v) {
            super(v);
            rootView = v;
            mClientName = v.findViewById(R.id.pendingReservationItem_clientNameTv);
            mField = v.findViewById(R.id.pendingReservationItem_fieldTv);
            mTime = v.findViewById(R.id.pendingReservationItem_timeTv);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PendingReservationsAdapter(ArrayList<ClientReservationsResponse> clientReservationsResponses, Context context, IAdatpterClickListener listener) {
        mClientReservationsResponses = clientReservationsResponses;
        this.listener = listener;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PendingReservationsAdapter.PendingReservationsViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v =  LayoutInflater.from(mContext)
                .inflate(R.layout.pending_reservation_item, parent, false);

        v.setOnClickListener(listener);

        return new PendingReservationsViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PendingReservationsViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        ClientReservationsResponse clientReservation = mClientReservationsResponses.get(position);
        holder.mClientName.setText(clientReservation.getTrackingId()+"");
        holder.mField.setText(clientReservation.getSportFieldId()+"");
        holder.mTime.setText(clientReservation.getTime());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mClientReservationsResponses.size();
    }

}