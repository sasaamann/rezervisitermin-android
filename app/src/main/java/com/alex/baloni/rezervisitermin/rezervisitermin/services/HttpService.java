package com.alex.baloni.rezervisitermin.rezervisitermin.services;

import com.alex.baloni.rezervisitermin.rezervisitermin.helpers.UnsafeOkHttpClient;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpService {

    public static final String BASE_URL = "http://laravel.rezervisitermin.com/";
    OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();


    public static HttpService _instance;

    Retrofit _retrofit;
    RezervisiTerminApi _apiService;

    public static HttpService getInstance(){
        if(_instance==null){
            _instance=new HttpService();
        }
        return _instance;
    }

    private HttpService(){
        _retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        _apiService =  _retrofit.create(RezervisiTerminApi.class);
    }

    public Retrofit getRetrofit() {
        return _retrofit;
    }

    public RezervisiTerminApi getApiService() {
        return _apiService;
    }
}
