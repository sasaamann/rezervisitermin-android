package com.alex.baloni.rezervisitermin.rezervisitermin.controllers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alex.baloni.rezervisitermin.rezervisitermin.RezervisiTerminApplication;

public abstract class BaseActivity extends AppCompatActivity {

    protected RezervisiTerminApplication application;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = (RezervisiTerminApplication) getApplication();
    }
}
