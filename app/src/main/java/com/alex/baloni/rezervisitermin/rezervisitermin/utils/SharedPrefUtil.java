package com.alex.baloni.rezervisitermin.rezervisitermin.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import static android.content.Context.MODE_PRIVATE;

public class SharedPrefUtil implements ISharedPref{
    private static final String USER_TOKEN      = "user_token";
    private static final String PREFS_NAME = "com.alex.baloni.rezervisitermin.rezervisitermin.utils.SharedPrefUtil";
    private Context context;

    public SharedPrefUtil(Context context){this.context = context;}

    private SharedPreferences getPreferences() {return getContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);}

    public Context getContext() {
        return context;
    }

    public  void clearPrefs() {
        getPreferences().edit().clear().apply();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        sharedPreferences.edit().clear().apply();
    }

    public  int getInt( String preferenceKey, int preferenceDefaultValue) {
        return getPreferences().getInt(preferenceKey, preferenceDefaultValue);
    }

    public  void putInt( String preferenceKey, int preferenceValue) {
        getPreferences().edit().putInt(preferenceKey, preferenceValue).apply();
    }

    public  void putBool( String preferenceKey, boolean preferenceValue) {
        getPreferences().edit().putBoolean(preferenceKey, preferenceValue).apply();
    }

    public String getString(String preferenceKey, String preferenceDefaultValue) {
        return getPreferences().getString(preferenceKey, preferenceDefaultValue);
    }

    public boolean getBool(String preferenceKey, boolean preferenceDefaultValue) {
        return getPreferences().getBoolean(preferenceKey, preferenceDefaultValue);
    }

    public  void putString(String preferenceKey, String preferenceValue) {
        getPreferences().edit().putString(preferenceKey, preferenceValue).apply();
    }

    public  void saveUserToken(String token) {
        putString(USER_TOKEN, token);
    }

    public String getUserToken() {
        return getString(USER_TOKEN, "");
    }
}
