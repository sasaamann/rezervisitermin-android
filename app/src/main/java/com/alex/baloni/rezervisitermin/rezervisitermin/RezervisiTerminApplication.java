package com.alex.baloni.rezervisitermin.rezervisitermin;

import android.app.Application;

import com.alex.baloni.rezervisitermin.rezervisitermin.infrastructure.Auth;

public class RezervisiTerminApplication extends Application {

    private Auth auth;

    @Override
    public void onCreate() {
        super.onCreate();
        auth = new Auth(this);
    }

    public Auth getAuth() {
        return auth;
    }
}
