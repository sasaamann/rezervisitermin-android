package com.alex.baloni.rezervisitermin.rezervisitermin.infrastructure;

public interface INavigator {

    void navigateToActivity(Class<?> classWhere);
}
