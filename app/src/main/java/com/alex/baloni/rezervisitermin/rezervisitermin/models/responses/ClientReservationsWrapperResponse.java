package com.alex.baloni.rezervisitermin.rezervisitermin.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ClientReservationsWrapperResponse extends BaseResponse{
    @SerializedName("data")
    @Expose
    private ArrayList<ClientReservationsResponse> data = null;


    public ArrayList<ClientReservationsResponse> getData() {
        return data;
    }

    public void setData(ArrayList<ClientReservationsResponse> data) {
        this.data = data;
    }
}
