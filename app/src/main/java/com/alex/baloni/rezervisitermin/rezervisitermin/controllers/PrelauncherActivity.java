package com.alex.baloni.rezervisitermin.rezervisitermin.controllers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.alex.baloni.rezervisitermin.rezervisitermin.infrastructure.ICheckForLoggedUser;
import com.alex.baloni.rezervisitermin.rezervisitermin.infrastructure.INavigator;
import com.alex.baloni.rezervisitermin.rezervisitermin.utils.ISharedPref;
import com.alex.baloni.rezervisitermin.rezervisitermin.utils.SharedPrefUtil;

public class PrelauncherActivity extends BaseActivity implements ICheckForLoggedUser, INavigator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean isExists = isUserExists(new SharedPrefUtil(getApplicationContext()));

        if(isExists){
            navigateToActivity(SplashActivity.class);
        }
        else{
            navigateToActivity(LoginActivity.class);
        }
    }

    @Override
    public boolean isUserExists(ISharedPref sharedPrefUtil) {
        String userToken = sharedPrefUtil.getUserToken();

        return userToken != null && !userToken.isEmpty();
    }

    @Override
    public void navigateToActivity(Class<?> classWhere) {
        startActivity(new Intent(PrelauncherActivity.this, classWhere));
        finish();
    }
}
