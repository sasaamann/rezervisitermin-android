package com.alex.baloni.rezervisitermin.rezervisitermin.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alex.baloni.rezervisitermin.rezervisitermin.R;
import com.alex.baloni.rezervisitermin.rezervisitermin.RezervisiTerminApplication;
import com.alex.baloni.rezervisitermin.rezervisitermin.adapters.PendingReservationsAdapter;
import com.alex.baloni.rezervisitermin.rezervisitermin.helpers.IAdatpterClickListener;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientReservationsResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientReservationsWrapperResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientResponseToReservationResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.services.HttpService;
import com.alex.baloni.rezervisitermin.rezervisitermin.services.RezervisiTerminApi;
import com.crowdfire.cfalertdialog.CFAlertDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OnlineFragment extends Fragment implements IAdatpterClickListener {

    private OnFragmentInteractionListener mListener;
    private String clientToken;
    private ArrayList<ClientReservationsResponse> mClientReservationsResponses ;
    private PendingReservationsAdapter pendingReservationsAdapter;
    private RezervisiTerminApi apiService;
    private ViewTreeObserver.OnPreDrawListener listener;

    //View bindings
    @BindView(R.id.onlineFragment_progressBar)
    ProgressBar progressBar;

    @BindView(R.id.onlineFragment_noReservationsTv)
    TextView noReservationsTv;

    @BindView(R.id.onlineFragment_reservationsRecyclerView)
    RecyclerView reservationsRecyclerView;

    @BindView(R.id.onlineFragment_errorTv)
    TextView errorTv;

    @BindView(R.id.onlineFragment_swipeRefresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_online, container, false);

        ButterKnife.bind(this, view);

        //Get client token
        clientToken = ((RezervisiTerminApplication) getActivity().getApplication()).getAuth().getUser().getClientToken();

        apiService = HttpService.getInstance().getApiService();

        //Set Adapter
        mClientReservationsResponses = new ArrayList<>();
        pendingReservationsAdapter = new PendingReservationsAdapter(mClientReservationsResponses, getContext(), this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reservationsRecyclerView.getContext(),
                layoutManager.getOrientation());

        reservationsRecyclerView.setLayoutManager(layoutManager);
        reservationsRecyclerView.addItemDecoration(dividerItemDecoration);

        reservationsRecyclerView.setAdapter(pendingReservationsAdapter);

        swipeRefreshLayout.setOnRefreshListener(() -> getPendingRequestsRetrofit(clientToken));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPendingRequestsRetrofitFromStart(clientToken);
    }

    private void getPendingRequestsRetrofitFromStart(String clientToken) {
        progressBar.setVisibility(View.VISIBLE);
        getPendingRequestsRetrofit(clientToken);
    }

    private void getPendingRequestsRetrofit(String clientToken) {

        errorTv.setVisibility(View.GONE);
        noReservationsTv.setVisibility(View.GONE);
        reservationsRecyclerView.setVisibility(View.GONE);

        apiService.getClientPendingReservations("Bearer " + clientToken, 1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ClientReservationsWrapperResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ClientReservationsWrapperResponse clientReservationsWrapperResponse) {
                        if(clientReservationsWrapperResponse.getData() == null || clientReservationsWrapperResponse.getData().size() <=0){
                            reservationsRecyclerView.setVisibility(View.GONE);
                            noReservationsTv.setVisibility(View.VISIBLE);
                            return;
                        }

                        reservationsRecyclerView.setVisibility(View.VISIBLE);

                        populateAndAnimateAdapter(clientReservationsWrapperResponse.getData());
                    }

                    @Override
                    public void onError(Throwable e) {
                        swipeRefreshLayout.setRefreshing(false);
                        progressBar.setVisibility(View.GONE);

                        noReservationsTv.setVisibility(View.GONE);
                        reservationsRecyclerView.setVisibility(View.GONE);

                        errorTv.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onComplete() {
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);

                        errorTv.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        int itemPosition = reservationsRecyclerView.getChildLayoutPosition(v);
        ClientReservationsResponse reservation = mClientReservationsResponses.get(itemPosition);
        areYouSureDialog(reservation.getTrackingId(), "Da li zelite da prihvatite termin za "+ reservation.getTrackingId()+ ", u "+ reservation.getTime() + "?", itemPosition);
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnlineFragment.OnFragmentInteractionListener) {
            mListener = (OnlineFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void areYouSureDialog(final String title, String message, final int position) {
        // Create Alert using Builder
        final CFAlertDialog.Builder builder = new CFAlertDialog.Builder(getContext())
                .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                .setTitle(title)
                .setMessage(message)
                .addButton("Prihvati", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        respondToTheReservation(clientToken, mClientReservationsResponses.get(position).getTrackingId(),"2");
//                        mClientReservationsResponses.remove(position);
//                        pendingReservationsAdapter.notifyItemRemoved(position);
//                        Toast.makeText(getContext(), title+ " Prihvacen", Toast.LENGTH_SHORT).show();
                    }
                })
                .addButton("Odbij", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        respondToTheReservation(clientToken, mClientReservationsResponses.get(position).getTrackingId(),"3");
//                        mClientReservationsResponses.remove(position);
//                        pendingReservationsAdapter.notifyItemRemoved(position);
//                        Toast.makeText(getContext(), title + " Odbijen", Toast.LENGTH_SHORT).show();
                    }
                });
        builder.show();
    }

    private void respondToTheReservation(String clientToken, String trackingId, String status) {
        RequestBody respondData = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("tracking_id", trackingId)
                .addFormDataPart("status", status)
                .build();

        apiService.respondToReservation("Bearer " + clientToken, respondData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ClientResponseToReservationResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ClientResponseToReservationResponse clientResponseToReservationResponse) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getPendingRequestsRetrofitFromStart(clientToken);
                        showErrorDialog("Neuspjesno slanje odgovora, molimo Vas pokusajte ponovo ili kontaktirajte podrsku.");
                    }

                    @Override
                    public void onComplete() {
                        getPendingRequestsRetrofitFromStart(clientToken);
                    }
                });
    }

    private void populateAndAnimateAdapter(ArrayList<ClientReservationsResponse> data){
        mClientReservationsResponses.clear();
        mClientReservationsResponses.addAll(data);

        pendingReservationsAdapter.notifyDataSetChanged();

        animateRecyclerData();
    }

    private void animateRecyclerData() {
        reservationsRecyclerView.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        listener = this;
                        reservationsRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

                        for (int i = 0; i < reservationsRecyclerView.getChildCount(); i++) {
                            View v = reservationsRecyclerView.getChildAt(i);
                            v.setAlpha(0.0f);
                            v.animate().alpha(1.0f)
                                    .setDuration(500)
                                    .setStartDelay(i * 50)
                                    .start();
                        }

                        return true;
                    }
                });
        reservationsRecyclerView.getViewTreeObserver().removeOnPreDrawListener(listener);
    }

    private void showErrorDialog(String errorMessage) {
        // Create Alert using Builder
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(getActivity())
                .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                .setTitle("Greška")
                .setMessage(errorMessage)
                .addButton("U redu", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }


}
