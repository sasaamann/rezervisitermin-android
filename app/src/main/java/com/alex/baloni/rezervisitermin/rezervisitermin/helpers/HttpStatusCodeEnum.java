package com.alex.baloni.rezervisitermin.rezervisitermin.helpers;

import java.util.HashMap;
import java.util.Map;

public enum HttpStatusCodeEnum {
    SUCESS(1),
    FAILURE(2);

    private int value;

    HttpStatusCodeEnum(int value)
    {
        if(value==1 || (value>=200 && value<300))
            this.value = 1;
        else{
            value =2;
        }
        // this.value = value;
    }
    public int getValue() {
        return value;
    }

    private static final Map<Integer, HttpStatusCodeEnum> intToTypeMap = new HashMap<Integer, HttpStatusCodeEnum>();
    static {
        for (HttpStatusCodeEnum type : HttpStatusCodeEnum.values()) {
            intToTypeMap.put(type.value, type);
        }
    }

    public static HttpStatusCodeEnum fromInt(int i) {
        if((i>=200 && i<300))
            return  HttpStatusCodeEnum.SUCESS;
        return HttpStatusCodeEnum.FAILURE;
    }
}
