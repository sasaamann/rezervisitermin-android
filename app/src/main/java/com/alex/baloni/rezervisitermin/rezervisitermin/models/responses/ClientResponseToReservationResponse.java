package com.alex.baloni.rezervisitermin.rezervisitermin.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientResponseToReservationResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private ClientReservationsResponse data = null;


    public ClientReservationsResponse getData() {
        return data;
    }

    public void setData(ClientReservationsResponse data) {
        this.data = data;
    }
}
