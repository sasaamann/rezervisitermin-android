package com.alex.baloni.rezervisitermin.rezervisitermin.controllers;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.alex.baloni.rezervisitermin.rezervisitermin.R;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.BaseResponse;

public class AddOfflineActivity extends BaseAuthenticatedActivity {

    @Override
    protected void onRezervisiTerminCreate(@Nullable Bundle savedInstanceState) {
        try{
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        catch (Exception ignored){}

        setContentView(R.layout.activity_add_offline);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
