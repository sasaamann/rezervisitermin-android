package com.alex.baloni.rezervisitermin.rezervisitermin.utils;

import android.content.Context;
import android.content.SharedPreferences;

public interface ISharedPref {

    void clearPrefs();

    void saveUserToken(String token);

    String getUserToken();

    Context getContext();

    void putInt(String preferenceKey, int preferenceValue);

    void putBool(String preferenceKey, boolean preferenceValue);

    void putString(String preferenceKey, String preferenceValue);

    int getInt(String preferenceKey, int preferenceDefaultValue);

    String getString(String preferenceKey, String preferenceDefaultValue);

    boolean getBool(String preferenceKey, boolean preferenceDefaultValue);
}
