package com.alex.baloni.rezervisitermin.rezervisitermin.controllers;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;


import com.alex.baloni.rezervisitermin.rezervisitermin.R;
import com.alex.baloni.rezervisitermin.rezervisitermin.RezervisiTerminApplication;
import com.alex.baloni.rezervisitermin.rezervisitermin.adapters.TabAdapter;
import com.alex.baloni.rezervisitermin.rezervisitermin.components.NoSwipeableViewPager;
import com.alex.baloni.rezervisitermin.rezervisitermin.fragments.OfflineFragment;
import com.alex.baloni.rezervisitermin.rezervisitermin.fragments.OnlineFragment;
import com.alex.baloni.rezervisitermin.rezervisitermin.infrastructure.INavigator;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.PageFragment;
import com.alex.baloni.rezervisitermin.rezervisitermin.utils.ISharedPref;
import com.alex.baloni.rezervisitermin.rezervisitermin.utils.SharedPrefUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnlineFragment.OnFragmentInteractionListener, OfflineFragment.OnFragmentInteractionListener, INavigator {


    @BindView(R.id.tabLayout) TabLayout tabLayout;

    @BindView(R.id.viewPager)
    NoSwipeableViewPager viewPager;

    private ArrayList<PageFragment> pageFragments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        pageFragments = generateFragments();

        FloatingActionButton fab =  findViewById(R.id.fab);
        fab.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, AddOfflineActivity.class)));

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Set first item on drawer selected
        navigationView.getMenu().getItem(0).setChecked(true);

        // Gert current page and change title
        int currentPage = viewPager.getCurrentItem();
        changeTitleBasedOnPage(currentPage);

        //Set Tab adapter
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());

        //Add fragments to adapter
        for (PageFragment pageFragment:pageFragments) {
            adapter.addFragment(pageFragment.getFragment(), pageFragment.getTabTitle());
        }

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);



        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                changeTitleBasedOnPage(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private ArrayList<PageFragment> generateFragments() {
        PageFragment onlineFragment = new PageFragment(new OnlineFragment(), "Online", "Online Rezervacije");
        PageFragment offlineFragment = new PageFragment(new OfflineFragment(), "Offline", "Offline Rezervacije");

        ArrayList<PageFragment> tempPageFragments = new ArrayList<>();
        tempPageFragments.add(onlineFragment);
        tempPageFragments.add(offlineFragment);

        return tempPageFragments;
    }

    private void changeTitleBasedOnPage(int currentPage) {
        PageFragment pageFragment = pageFragments.get(currentPage);

        try{
            getSupportActionBar().setTitle(pageFragment.getActionBarTittle());
        }
        catch (Exception ignored){}
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout){
            logoutUserAndNavigateToLogin(new SharedPrefUtil(getApplicationContext()));
        }
        else if(id == R.id.nav_contact){
            navigateToActivity(ContactActivity.class);
        }

        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logoutUserAndNavigateToLogin(ISharedPref sharedPrefUtil) {
        sharedPrefUtil.clearPrefs();
        ((RezervisiTerminApplication) getApplication()).getAuth().logoutUser();

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        //Clear all activities on top
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void navigateToActivity(Class<?> classWhere) {
        startActivity(new Intent(MainActivity.this, classWhere));
    }
}
