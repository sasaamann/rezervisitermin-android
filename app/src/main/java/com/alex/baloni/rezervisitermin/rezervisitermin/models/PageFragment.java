package com.alex.baloni.rezervisitermin.rezervisitermin.models;

import android.support.v4.app.Fragment;

public class PageFragment {

    private Fragment fragment;

    private String TabTitle;

    private String ActionBarTittle;

    public PageFragment(Fragment fragment, String tabTitle, String actionBarTittle) {
        this.fragment = fragment;
        TabTitle = tabTitle;
        ActionBarTittle = actionBarTittle;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getTabTitle() {
        return TabTitle;
    }

    public void setTabTitle(String tabTitle) {
        TabTitle = tabTitle;
    }

    public String getActionBarTittle() {
        return ActionBarTittle;
    }

    public void setActionBarTittle(String actionBarTittle) {
        ActionBarTittle = actionBarTittle;
    }
}
