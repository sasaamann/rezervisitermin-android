package com.alex.baloni.rezervisitermin.rezervisitermin.services;

import com.alex.baloni.rezervisitermin.rezervisitermin.models.User;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.requests.TokenRequest;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientReservationsResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientReservationsWrapperResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.ClientResponseToReservationResponse;
import com.alex.baloni.rezervisitermin.rezervisitermin.models.responses.TokenResponse;

import java.util.ArrayList;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RezervisiTerminApi {

    //POST Methods ------------

    @POST("oauth/token")
    Observable<TokenResponse> getToken(@Body RequestBody body);

    @POST("api/client/reservation/response")
    Observable<ClientResponseToReservationResponse> respondToReservation(@Header("Authorization") String clientToken, @Body RequestBody body);



    //GET Methods ------------

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("api/user")
    Observable<User> getUserData(@Header("Authorization") String clientToken);

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("api/client/reservations")
    Observable<ClientReservationsWrapperResponse> getClientPendingReservations(@Header("Authorization") String clientToken, @Query("status") int status);


}
